package io.gitlab.lsolovyeva.order.service;

import io.gitlab.lsolovyeva.order.dto.OrderRequest;
import io.gitlab.lsolovyeva.order.model.Order;
import io.gitlab.lsolovyeva.order.repository.OrderRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class OrderService {

    public final OrderRepository orderRepository;

    @Transactional
    public void addOrder(OrderRequest orderRequest) {
        Order order = new Order();
        order.setName(orderRequest.getName());
        order.setUserId(orderRequest.getUserId());
        order.setCreateDate(LocalDateTime.now());
        orderRepository.save(order);
    }
}
