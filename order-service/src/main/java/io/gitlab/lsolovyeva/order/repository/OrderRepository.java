package io.gitlab.lsolovyeva.order.repository;

import io.gitlab.lsolovyeva.order.model.Order;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
//@Transactional(readOnly = true)
public interface OrderRepository extends CrudRepository<Order, Long> {

}
