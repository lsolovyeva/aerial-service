package io.gitlab.lsolovyeva.order.controller;

import io.gitlab.lsolovyeva.order.dto.OrderRequest;
import io.gitlab.lsolovyeva.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller //RestController not working with Thymeleaf
@RequestMapping("/order")
public class OrderController {

    @Autowired
    public OrderService orderService;

    @GetMapping("")
    public String getIndex(Model model, @RequestParam(value = "userId", required = true) Long userId) {
        OrderRequest orderDto = OrderRequest.builder().userId(userId).build();
        model.addAttribute("order", orderDto);
        return "index";
    }

    @PostMapping(value = "/create")
    public ResponseEntity addOrderNew(@ModelAttribute("order") OrderRequest orderRequest) {
        orderService.addOrder(orderRequest);
        log.info("Creating new order.");
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
