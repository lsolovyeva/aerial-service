package io.gitlab.lsolovyeva.order.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "order", schema = "orders", catalog = "aerialdb")
public class Order {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id;

    @Column(name = "name", nullable = false)
    //@NotEmpty
    //@Size(max = 128)
    private String name;

    @Column(name = "user_id", nullable = false)
    //@NotEmpty
    //@Size(max = 128)
    private Long userId;

    @Column(name = "create_date", nullable = false)
    private LocalDateTime createDate;
}
