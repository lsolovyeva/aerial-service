package io.gitlab.lsolovyeva.gateway.controller;

import io.gitlab.lsolovyeva.gateway.dto.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Slf4j
@Controller //RestController not working with Thymeleaf
@RequestMapping("/gateway")
public class GatewayController {

    @Value("${service.order.url:order}")
    String orderServiceUrl;

    @Value("${service.review.url:comment}")
    String reviewServiceUrl;

    @GetMapping("")
    public String getIndex(Model model, @RequestParam(value = "userId", required = true) Long userId) {
        UserDto userDto = UserDto.builder().id(userId).build();
        model.addAttribute("user", userDto);
        System.out.println(userId);
        return "index";
    }

    @PostMapping(value = "/action", params = "action=order")
    public String addOrder(@ModelAttribute("user") UserDto userDto, RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("userId", userDto.getId());
        return "redirect:" + "http://localhost:8082/" + orderServiceUrl;
    }


    @PostMapping(value = "/action", params = "action=comment")
    public String addComment(@ModelAttribute("user") UserDto userDto, RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("userId", userDto.getId());
        return "redirect:" + "http://localhost:8083/" + reviewServiceUrl;
    }
}
