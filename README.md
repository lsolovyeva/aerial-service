### Java Spring template project
This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

### Docker
1. Start a container (=postgres instance) with name=aerial-postgres

   `docker run --name aerial-postgres -e POSTGRES_PASSWORD=admin -e POSTGRES_USER=admin -e POSTGRES_DB=aerialdb -d -p 5432:5432 postgres`

   It will be then shown in Docker Desktop.

2. In Docker Desktop go to `Containers`-> `aerial-postgres`-> `Terminal` and run:
   `psql -h localhost -U admin aerialdb` to execute commands in DB.
   Example: `\l` - get list of databases.

### How to run application
1. Run Docker (see Docker step) with DB
2. Start Kafka server:
   `cd /Users/ludmila/Desktop/IdeaProjects/aerial-service/communication-service`
   `docker-compose up -d`
   * You can use Kafka Tool GUI utility visualize data across partitions and view active consumers.
3. Multiple services run configuration is placed in `.run/Aerial5Service.run.xml`. 
It allows running 5 applications simultaneously (by using compound configuration): Login Application, Gateway Application,
Order Application, Review Application, Communication Application.

### Database
Catalog=DB=aerialdb, Schema=namespace=logins

### Kafka
For local development needs it is enough to use a single node Kafka broker.
In `communication-service/docker-compose.yml` we start a Zookeeper server and an Apache Kafka server after that.
Kafka allows microservices to asynchronously send millions e-mails to the users per day in real time.
Kafka topic may have multiple partitions to allow parallel consumption of the messages.

### Tech stack
Java 17, PostgreSQL 16, Kafka.
Spring Framework modules: Boot 3, Web, Data JPA, Mail/Thymeleaf, Test.
Testcontainers.

#### TODOs
1. Use records (for OrderService)
2. Add Dockerfile and docker-compose file to docker folder
3. Check Kafka connection for Communication service after docker-compose up

### News

- Integrated with GitLab Code Quality tool.
   Now the checks are shown for MRs:

![](images/code-quality-check.png)

### How to Run Java App on Docker

1. Build the Docker Image for Communication Service from Dockerfile:
`cd /aerial-service/communication-service`
`docker build -t my-java-app .`
2. Run the Docker Container
`docker run -p 8084:8084 my-java-app`

Or use docker-compose to run Communication service + Database + Kafka:
`cd aerial-service/communication-service`  (`cd /aerial-service/docker`)
`docker-compose up`
