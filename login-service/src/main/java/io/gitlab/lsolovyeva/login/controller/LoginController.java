package io.gitlab.lsolovyeva.login.controller;

import io.gitlab.lsolovyeva.login.dto.UserDto;
import io.gitlab.lsolovyeva.login.exception.UserAlreadyExistException;
import io.gitlab.lsolovyeva.login.model.User;
import io.gitlab.lsolovyeva.login.service.UserService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Slf4j
@Controller //RestController not working with Thymeleaf
@RequestMapping("/api/user")
public class LoginController {
    private final UserService userService;

    @Value("${service.gateway.url:gateway}")
    String gatewayServiceUrl;

    public LoginController(UserService userService) {
        this.userService = userService;
    }

    // http://localhost:8080//login is generated with Spring, see io.gitlab.lsolovyeva.aerial.config.SecurityConfig
    // If login was success -> redirect to /api/user/index
    // If login fails -> redirect to /api/user/error

    @GetMapping("/index")
    public String redirectToGatewayServiceOnSuccessLogin(RedirectAttributes redirectAttributes) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        User user = (User) authentication.getPrincipal();
        redirectAttributes.addAttribute("userId", user.getId());
        return "redirect:" + "http://localhost:8081/" + gatewayServiceUrl;
    }

    @GetMapping("/error")
    public String getError() {
        return "error";
    }

    @GetMapping("/registration")
    public String showRegistrationForm(WebRequest request, Model model) {
        UserDto userDto = new UserDto();
        model.addAttribute("user", userDto);
        return "registration";
    }

    @PostMapping("/registration")
    public ModelAndView registerUserAccount(@ModelAttribute("user") @Valid UserDto userDto, HttpServletRequest request, Errors errors) {
        log.debug("Registering user account with information: {}", userDto);
        try {
            User registered = userService.registerNewUserAccount(userDto);
        } catch (UserAlreadyExistException uaeEx) {
            ModelAndView mav = new ModelAndView("registration", "user", userDto);

            mav.addObject("message", "An account for that username/email already exists.");
            return mav;
        }
        return new ModelAndView("successRegister", "user", userDto);
    }
}
