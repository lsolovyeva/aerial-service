package io.gitlab.lsolovyeva.login.service;

import io.gitlab.lsolovyeva.login.dto.UserDto;
import io.gitlab.lsolovyeva.login.model.User;

public interface UserService {
    User registerNewUserAccount(UserDto userDto);

}
