package io.gitlab.lsolovyeva.login.dto;

import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    @NotNull
    @NotEmpty
    //@Size(min = 1, message = "{Size.userDto.firstName}")
    private String username;

    @NotNull
    @NotEmpty
    // @ValidPassword
    private String password;

    @NotNull
    @NotEmpty
    //@ValidEmail
    //@Size(min = 1, message = "{Size.userDto.email}")
    private String email;

}
