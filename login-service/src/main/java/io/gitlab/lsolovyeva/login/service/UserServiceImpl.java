package io.gitlab.lsolovyeva.login.service;

import io.gitlab.lsolovyeva.login.dto.UserDto;
import io.gitlab.lsolovyeva.login.exception.UserAlreadyExistException;
import io.gitlab.lsolovyeva.login.model.User;
import io.gitlab.lsolovyeva.login.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    // https://github.com/Baeldung/spring-security-registration/tree/master/src/main/java/com/baeldung
    @Override
    public User registerNewUserAccount(UserDto userDto) throws UserAlreadyExistException {
        if (emailExists(userDto.getEmail())) {
            throw new UserAlreadyExistException("There is an account with that email address: " + userDto.getEmail());
        }

        User user = new User();
        user.setUsername(userDto.getUsername());
        user.setPassword("{noop}" + userDto.getPassword());
        user.setEmail(userDto.getEmail());
        return userRepository.save(user);
    }

    private boolean emailExists(String email) {
        return userRepository.findByEmail(email) != null;
    }
}
