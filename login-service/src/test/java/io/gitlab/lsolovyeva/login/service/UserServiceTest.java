package io.gitlab.lsolovyeva.login.service;

import io.gitlab.lsolovyeva.login.dto.UserDto;
import io.gitlab.lsolovyeva.login.model.User;
import io.gitlab.lsolovyeva.login.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    private User testUser;

    @BeforeEach
    void setUp() {
        userService = new UserServiceImpl(userRepository);
        testUser = User.builder().username("testUser").build();
        when(userRepository.save(any())).thenReturn(testUser);

    }

    @Test
    void addNewDish() {
        UserDto userDto = UserDto.builder().username("testUser").email("testEmail").build();
        User user = userService.registerNewUserAccount(userDto);
        assertEquals(user.getUsername(), userDto.getUsername());
    }
}
