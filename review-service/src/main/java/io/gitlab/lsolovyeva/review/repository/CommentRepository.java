package io.gitlab.lsolovyeva.review.repository;

import io.gitlab.lsolovyeva.review.model.Comment;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
//@Transactional(readOnly = true)
public interface CommentRepository extends CrudRepository<Comment, Long> {

}
