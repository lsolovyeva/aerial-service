package io.gitlab.lsolovyeva.review.service;

import io.gitlab.lsolovyeva.review.dto.CommentDto;
import io.gitlab.lsolovyeva.review.model.Comment;
import io.gitlab.lsolovyeva.review.repository.CommentRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@AllArgsConstructor
public class CommentService {

    public final CommentRepository commentRepository;

    @Transactional
    public void addComment(CommentDto commentDto) {
        Comment comment = new Comment();
        comment.setText(commentDto.getText());
        comment.setUserId(commentDto.getUserId());
        comment.setCreateDate(LocalDateTime.now());
        commentRepository.save(comment);
    }
}
