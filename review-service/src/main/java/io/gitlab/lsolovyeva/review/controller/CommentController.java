package io.gitlab.lsolovyeva.review.controller;

import io.gitlab.lsolovyeva.review.dto.CommentDto;
import io.gitlab.lsolovyeva.review.service.CommentService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Slf4j
@Controller //RestController not working with Thymeleaf
@AllArgsConstructor
@RequestMapping("/comment")
public class CommentController {

    public final CommentService commentService;

    @GetMapping("")
    public String getIndex(Model model, @RequestParam(value = "userId", required = true) Long userId) {
        CommentDto commentDto = CommentDto.builder().userId(userId).build();
        model.addAttribute("comment", commentDto);
        return "index";
    }

    @PostMapping(value = "/add")
    public ResponseEntity addNewComment(@ModelAttribute("comment") CommentDto commentDto) {
        commentService.addComment(commentDto);
        log.info("Adding new comment.");
        return new ResponseEntity(HttpStatus.CREATED);
    }
}
