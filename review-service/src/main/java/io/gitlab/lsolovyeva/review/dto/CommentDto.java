package io.gitlab.lsolovyeva.review.dto;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class CommentDto {

    private Long userId;

    private String text;

    private LocalDateTime createDate;

}
