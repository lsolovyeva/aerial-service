package io.gitlab.lsolovyeva.communication.repository;

import io.gitlab.lsolovyeva.communication.CommunicationApplication;
import io.gitlab.lsolovyeva.communication.model.Communication;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDateTime;

import static io.gitlab.lsolovyeva.communication.model.MessageType.ORDER_SMS;
import static org.junit.jupiter.api.Assertions.assertNotNull;

//@DataJpaTest
@ActiveProfiles("test")
@Testcontainers(disabledWithoutDocker = true)
@SpringBootTest
//@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = {PostgreSQLContainerInitializer.class}, classes = {CommunicationApplication.class})
class CommunicationRepositoryTest {

    @Autowired
    private CommunicationRepository communicationRepository;

    @Test
    void test() {
        Communication communication = communicationRepository.save(new Communication(1L, "text", 1L, ORDER_SMS, LocalDateTime.now()));
        assertNotNull(communication);
    }
}
