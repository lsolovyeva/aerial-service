package io.gitlab.lsolovyeva.communication.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommunicationDto {

    private String text;

    private Long userId;

    private String messageType;

}
