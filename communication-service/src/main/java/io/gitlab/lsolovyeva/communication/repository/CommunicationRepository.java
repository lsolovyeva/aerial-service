package io.gitlab.lsolovyeva.communication.repository;

import io.gitlab.lsolovyeva.communication.model.Communication;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CommunicationRepository extends CrudRepository<Communication, Long> {

}