package io.gitlab.lsolovyeva.communication.service;

import io.gitlab.lsolovyeva.communication.model.Communication;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class EmailService {

    private final JavaMailSender mailSender;

    public void sendEmail() {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom("dianasweet1203@gmail.com");
        message.setTo("lsolovyeva09@gmail.com");
        message.setText("Aerial Hello!");
        message.setSubject("Hello from Aerial!");

        try {
            mailSender.send(message);
            log.info("Mail has been sent successfully.");
        } catch (Exception e) {
            log.error("Error while sending a message.", e);
        }
    }

    public void sendEmail(Communication communication) {
        /* Stub */
        log.info("Stubbed e-mail has been sent.");

        // message.setTo(""); // TODO: replace with user email from DB
        // message.setText(!communication.getText().isEmpty() ? communication.getText() : "Aerial Hello!");
        // message.setSubject("Hello from Aerial!"); // TODO: receive a subject from the request
    }
}
