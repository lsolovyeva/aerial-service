package io.gitlab.lsolovyeva.communication.service;

import io.gitlab.lsolovyeva.communication.dto.CommunicationDto;
import io.gitlab.lsolovyeva.communication.kafka.KafkaProducerService;
import io.gitlab.lsolovyeva.communication.model.Communication;
import io.gitlab.lsolovyeva.communication.model.MessageType;
import io.gitlab.lsolovyeva.communication.repository.CommunicationRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

import static io.gitlab.lsolovyeva.communication.kafka.KafkaTopicConfig.DEFAULT_KAFKA_TOPIC;

@Service
@AllArgsConstructor
public class CommunicationService {

    public final CommunicationRepository communicationRepository;
    public final KafkaProducerService kafkaProducerService;

    @Transactional
    public void sendMessage(CommunicationDto communicationDto) {
        Communication communication = new Communication();
        communication.setText(communicationDto.getText());
        communication.setUserId(communicationDto.getUserId());
        communication.setType(MessageType.valueOf(communicationDto.getMessageType()));
        communication.setCreateDate(LocalDateTime.now());
        communicationRepository.save(communication);
        kafkaProducerService.send(DEFAULT_KAFKA_TOPIC, communication);
    }
}
