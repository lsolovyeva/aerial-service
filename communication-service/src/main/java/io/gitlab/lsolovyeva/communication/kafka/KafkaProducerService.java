package io.gitlab.lsolovyeva.communication.kafka;

import io.gitlab.lsolovyeva.communication.model.Communication;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static io.gitlab.lsolovyeva.communication.kafka.KafkaTopicConfig.DEFAULT_KAFKA_TOPIC;

@Slf4j
@Service
@AllArgsConstructor
public class KafkaProducerService {

    private final KafkaTemplate<String, Object> kafkaTemplate;

    public void send(String topic, Communication message) {
        //log.info("sending payload='{}' to topic='{}'", payload, topic);
        //kafkaTemplate.send(topic, payload);

        kafkaTemplate.send(topic, "key", message)
                .whenComplete((result, ex) -> {
                    if (ex == null) {
                        log.info("Message sent to topic {}: {}", topic, message);
                    } else {
                        log.error("Failed to send message", ex);
                    }
                });

    }
}
