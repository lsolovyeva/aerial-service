package io.gitlab.lsolovyeva.communication.model;

public enum MessageType {
    ORDER_EMAIL,
    ORDER_SMS,
    REVIEW_EMAIL
}
