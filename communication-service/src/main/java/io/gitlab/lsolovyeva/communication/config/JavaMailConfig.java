package io.gitlab.lsolovyeva.communication.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

//https://www.baeldung.com/spring-email
//https://myaccount.google.com/apppasswords + account security configs
@Configuration
public class JavaMailConfig {

    @Bean(name = "mailSender")
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost("smtp.gmail.com");
        mailSender.setPort(587); // port 465 (for SSL), or port 587 (for TLS)

        mailSender.setUsername("dianasweet1203@gmail.com");
        mailSender.setPassword("ulcincvjdkylcckt");

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", "smtp");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.debug", "true");

        return mailSender;
    }
}
