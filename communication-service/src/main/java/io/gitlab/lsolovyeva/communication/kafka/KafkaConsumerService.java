package io.gitlab.lsolovyeva.communication.kafka;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.gitlab.lsolovyeva.communication.model.Communication;
import io.gitlab.lsolovyeva.communication.service.EmailService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static io.gitlab.lsolovyeva.communication.kafka.KafkaTopicConfig.DEFAULT_KAFKA_TOPIC;

@Slf4j
@Service
@AllArgsConstructor
public class KafkaConsumerService {

    private final EmailService emailService;
    private final ObjectMapper objectMapper;

    @Transactional
    @KafkaListener(topics = DEFAULT_KAFKA_TOPIC, groupId = "aerial-group-id",
            properties = {"spring.json.value.default.type=io.gitlab.lsolovyeva.communication.model.Communication"})
    public void receive(Communication communication) { //ConsumerRecord<String, Object> consumerRecord) {
        try {
            //Communication communication = objectMapper.readValue(consumerRecord.value(), Communication.class);
            emailService.sendEmail(communication);
            log.info("Kafka message consumed: '{}'", communication);
        } catch (Exception e) { //JsonProcessingException
            log.error("Error while receiving a message from Kafka:", e);
            //throw new RuntimeException(e);
        }
    }
}
