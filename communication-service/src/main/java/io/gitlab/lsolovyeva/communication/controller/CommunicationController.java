package io.gitlab.lsolovyeva.communication.controller;

import io.gitlab.lsolovyeva.communication.dto.CommunicationDto;
import io.gitlab.lsolovyeva.communication.service.CommunicationService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@AllArgsConstructor
@RequestMapping("/communication")
public class CommunicationController {

    private final CommunicationService communicationService;

    @PostMapping("/mail")
    public String sendMail(@RequestBody CommunicationDto communicationDto) {
        log.info("Received email sending request: {}", communicationDto);

        communicationService.sendMessage(communicationDto);
        return "Success!";
    }
}
