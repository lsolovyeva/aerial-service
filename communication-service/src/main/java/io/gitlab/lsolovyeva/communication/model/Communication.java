package io.gitlab.lsolovyeva.communication.model;

import jakarta.persistence.*;
import lombok.*;

import java.time.LocalDateTime;

@AllArgsConstructor
@NoArgsConstructor
@ToString
@Getter
@Setter
@Entity
@Table(name = "communication", schema = "communications", catalog = "aerialdb")
public class Communication {

    @Id
    @Column(unique = true, nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Long id; // messageId

    @Column(name = "text", nullable = false)
    private String text;

    @Column(name = "user_id", nullable = false)
    private Long userId;

    @Enumerated(EnumType.STRING)
    private MessageType type;

    @Column(name = "create_date", nullable = false)
    private LocalDateTime createDate;

}
